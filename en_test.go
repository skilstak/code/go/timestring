package timestring_test

import (
	"fmt"
	"time"

	ts "gitlab.com/skilstak/code/go/timestring"
)

func ExampleEN() {

	t, _ := time.Parse("January 2, 2006 3:04:05am mst", "December 10, 1967 6:30:28am mst")

	fmt.Println(ts.EN("blog/YEAR/MONTH/DAY", t))
	fmt.Println(ts.EN("blog-LONGMONTH-YEAR", t))
	fmt.Println(ts.EN("blog-LOWERLONGMONTH-YEAR", t))
	fmt.Println(ts.EN("blog-SHORTMONTH-YEAR", t))
	fmt.Println(ts.EN("blog-LOWERSHORTMONTH-YEAR", t))
	fmt.Println(ts.EN("file-EPOCH.md", t))
	fmt.Println(ts.EN("file-YEAR-MONTH-DAY-HOUR-MINUTE-SECOND.md", t))
	fmt.Println(ts.EN("YEAR-IMONTH-IDAY IHOUR:IMINUTE:ISECOND", t))
	fmt.Println(ts.EN("session-IHOUR", t))
	fmt.Println(ts.EN("session-HOURLOWERAMPM", t))
	fmt.Println(ts.EN("MONTHIMONTH DAYIDAY HOURIHOUR MINUTEIMINUTE SECONDISECOND", t))

	// Output:
	//
	// blog/1967/12/10
	// blog-December-1967
	// blog-december-1967
	// blog-Dec-1967
	// blog-dec-1967
	// file--65035772.md
	// file-1967-12-10-6-30-28.md
	// 1967-12-10 06:30:28
	// session-06
	// session-6am
	// 1212 1010 606 3030 2828
}
