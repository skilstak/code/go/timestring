package timestring_test

import (
	"fmt"
	"regexp"
	"testing"

	ts "gitlab.com/skilstak/code/go/timestring"
)

func TestEpochify_dot(t *testing.T) {
	s := "some.md"
	se := ts.Epochify(s)
	fmt.Printf("# %v -> %v\n", s, se)
	if m, _ := regexp.MatchString(`^some\-[0-9]{10,}\.md$`, se); !m {
		t.Errorf("doesn't look like a match: %v, %v\n", s, se)
	}
}

func TestEpochify_nodot(t *testing.T) {
	s := "some"
	se := ts.Epochify(s)
	fmt.Printf("# %v -> %v\n", s, se)
	if m, _ := regexp.MatchString(`^some\-[0-9]{10,}$`, se); !m {
		t.Errorf("doesn't look like a match: %v, %v\n", s, se)
	}
}

func ExampleEpochify() {
	fmt.Println(ts.Epochify("some.md"))
	fmt.Println(ts.Epochify("some"))
	// some-1563213180.md
	// some-1563213180
}
