package timestring

import (
	"fmt"
	"strings"
	"time"
)

// EN is the English version of the timestring keywords:
//
//     LOWERLONGMONTH   january
//     LOWERSHORTMONTH  Jan
//     LONGMONTH        January
//     SHORTMONTH       Jan
//     LOWERZONE        mst
//     LOWERAMPM        pm
//     EPOCH            1563215203
//     IMONTH           01
//     IDAY             02
//     IHOUR            15
//     IMINUTE          04
//     ISECOND          05
//     YEAR             2006
//     MONTH            1
//     DAY              2
//     HOUR             3
//     MINUTE           4
//     SECOND           5
//     ZONE             MST
//     AMPM             MST
//
func EN(s string, t time.Time) string {
	longmonth := t.Format("January")
	shortmonth := t.Format("Jan")
	// sure there is likely a faster way, this works for now
	r := strings.NewReplacer(
		"LOWERLONGMONTH", strings.ToLower(longmonth),
		"LOWERSHORTMONTH", strings.ToLower(shortmonth),
		"LONGMONTH", longmonth,
		"SHORTMONTH", shortmonth,
		"LOWERZONE", t.Format("mst"),
		"LOWERAMPM", t.Format("pm"),
		"EPOCH", fmt.Sprint(t.Unix()),
		"IMONTH", t.Format("01"),
		"IDAY", t.Format("02"),
		"IHOUR", t.Format("15"),
		"IMINUTE", t.Format("04"),
		"ISECOND", t.Format("05"),
		"YEAR", t.Format("2006"),
		"MONTH", t.Format("1"),
		"DAY", t.Format("2"),
		"HOUR", t.Format("3"),
		"MINUTE", t.Format("4"),
		"SECOND", t.Format("5"),
		"ZONE", t.Format("MST"),
		"AMPM", t.Format("PM"),
	)
	return r.Replace(s)
}
