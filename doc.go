// Package timestring is way to represent time and date information in a string
// format that is more readable to those maintaining it, for example in
// configuration files and such.
//
// The epochify utility command is also included with this package.
package timestring
