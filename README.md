# International Time String Formatting

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/skilstak/code/go/timestring)](https://goreportcard.com/report/gitlab.com/skilstak/code/go/timestring) [![Coverage](https://gocover.io/_badge/gitlab.com/skilstak/code/go/timestring)](https://gocover.io/gitlab.com/skilstak/code/go/timestring) [![GoDoc](https://godoc.org/gitlab.com/skilstak/code/go/timestring?status.svg)](https://godoc.org/gitlab.com/skilstak/code/go/timestring)

A way to write time formats more easily in configuration and other files.
