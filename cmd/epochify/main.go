package main

import (
	"fmt"
	"os"
	"strings"

	ts "gitlab.com/skilstak/code/go/timestring"
)

const usage = `
usage: epochify STRING
       epochify STRING.SUF
`

func main() {
	if len(os.Args) < 2 {
		fmt.Println(strings.TrimSpace(usage))
		os.Exit(1)
	}
	fmt.Print(ts.Epochify(os.Args[1]))
}
