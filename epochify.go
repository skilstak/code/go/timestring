package timestring

import (
	"fmt"
	"strings"
	"time"
)

// Epochify simply addes the seconds since UNIX epoch (see time.Time.Unix) to
// the end of the file name but before the last dot.
func Epochify(path string) string {
	i := strings.LastIndex(path, ".")
	if i >= 0 {
		return fmt.Sprintf("%v-%v%v", path[0:i], time.Now().Unix(), path[i:])
	}
	return fmt.Sprintf("%v-%v", path, int32(time.Now().Unix()))
}
